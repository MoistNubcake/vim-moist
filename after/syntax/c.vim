" For highlight groups see 'syncolor.vim' 

syntax match phpDocumentorString "@[^\*/]*"  oneline contained contains=phpDocumentorParam,phpDocumentorGeneral,phpDocumentorType containedin=.*Comment.* 

syntax match phpDocumentorName2Desc         "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorDescription
syntax match phpDocumentorType2Name2Desc    "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorName2Desc
syntax match phpDocumentorType2Desc         "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorDescription

syntax match phpDocumentorGeneral   "@\w\+"     contained skipwhite nextgroup=phpDocumentorDescription
syntax match phpDocumentorParam     "@param"    contained skipwhite nextgroup=phpDocumentorType2Name2Desc
syntax match phpDocumentorType      "@return"   contained skipwhite nextgroup=phpDocumentorType2Desc
syntax match phpDocumentorType      "@see"      contained skipwhite nextgroup=phpDocumentorName2Desc

syntax cluster phpDocumentor contains=phpDocumentorString,phpDocumentorDescription,phpDocumentorName2Desc,phpDocumentorType2Name2Desc,phpDocumentorType2Desc,phpDocumentorGeneral,phpDocumentorParam,phpDocumentorType,phpDocumentorType

