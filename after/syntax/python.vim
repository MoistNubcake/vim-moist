syntax match phpDocumentorString "@.*" contained contains=phpDocumentorParam,phpDocumentorGeneral,phpDocumentorType containedin=.*Comment.*,pythonString

syntax match phpDocumentorDescription       ".*" contained containedin=phpDocumentorString
syntax match phpDocumentorName2Desc         "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorDescription
syntax match phpDocumentorType2Name2Desc    "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorName2Desc
syntax match phpDocumentorType2Desc         "\<\w\+\(\_$\|\>\)" contained skipwhite nextgroup=phpDocumentorDescription

syntax match phpDocumentorGeneral   "@\w\+"     contained skipwhite nextgroup=phpDocumentorDescription
syntax match phpDocumentorParam     "@param"    contained skipwhite nextgroup=phpDocumentorType2Name2Desc
syntax match phpDocumentorType      "@return"   contained skipwhite nextgroup=phpDocumentorType2Desc
syntax match phpDocumentorType      "@see"      contained skipwhite nextgroup=phpDocumentorName2Desc

syntax cluster phpDocumentor contains=phpDocumentorString,phpDocumentorDescription,phpDocumentorName2Desc,phpDocumentorType2Name2Desc,phpDocumentorType2Desc,phpDocumentorGeneral,phpDocumentorParam,phpDocumentorType,phpDocumentorType

" Fix for ALLBUT as there is no NONEBUT equivalent
" (pythonMatrixMultiply and pythonAttribute would contain the phpDocumentors)
if hlexists("pythonMatrixMultiply")
syntax clear pythonMatrixMultiply
syn match   pythonMatrixMultiply
      \ "\%(\w\|[])]\)\s*@"
      \ contains=ALLBUT,pythonDecoratorName,pythonDecorator,pythonFunction,pythonDoctestValue,@phpDocumentor
      \ transparent
syn match   pythonMatrixMultiply
      \ "[^\\]\\\s*\n\%(\s*\.\.\.\s\)\=\s\+@"
      \ contains=ALLBUT,pythonDecoratorName,pythonDecorator,pythonFunction,pythonDoctestValue,@phpDocumentor
      \ transparent
syn match   pythonMatrixMultiply
      \ "^\s*\%(\%(>>>\|\.\.\.\)\s\+\)\=\zs\%(\h\|\%(\h\|[[(]\).\{-}\%(\w\|[])]\)\)\s*\n\%(\s*\.\.\.\s\)\=\s\+@\%(.\{-}\n\%(\s*\.\.\.\s\)\=\s\+@\)*"
      \ contains=ALLBUT,pythonDecoratorName,pythonDecorator,pythonFunction,pythonDoctestValue,@phpDocumentor
      \ transparent
endif

if hlexists("pythonAttribute")
syntax clear pythonAttribute

syn match   pythonAttribute	/\.\h\w*/hs=s+1
	\ contains=ALLBUT,pythonBuiltin,pythonFunction,pythonAsync,@phpDocumentor
	\ transparent
endif
