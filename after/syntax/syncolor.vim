" This script is needed when for when 'syntax reset' is called
" 'syntax reset' is called when for example changing colorschemes which would
" normally delete highlight groups that are not in the colorscheme itself
hi link phpDocumentorString Comment

hi link phpDocumentorType2Name2Desc Type
hi link phpDocumentorType2Desc      Type
hi link phpDocumentorName2Desc      Label
hi link phpDocumentorDescription    Comment

hi link phpDocumentorGeneral    Label
hi link phpDocumentorParam      phpDocumentorGeneral
hi link phpDocumentorType       phpDocumentorGeneral

if get(b:, 'MyConcealColorLink', 0)
    execute 'hi! link Conceal ' . b:MyConcealColorLink
endif
