if exists("s:run")
    finish
endif

"       *** KEY MOVEMENT *** 

" REMAP: h  j  l  k
" TO:    j  k  l  i                                                                                   
                                                                                                      
" NOTE: shift-direction  : small jump, leave in normal                                                
"       leader-direction : end jump  , go into insert mode                                            
                                                                                                      
" 1) left; 2) right; 3) down; 4) up                                                                   
let s:mvdirection = [ "all",{
    \ 'j' : 'h', 'J' : '0',     '<leader>j' : '0i',
    \ 'l' : 'l', 'L' : '$',     '<leader>l' : '$a',
    \ 'k' : 'j', 'K' : '<c-d>', '<leader>k' : 'G$a',
    \ 'i' : 'k', 'I' : '<c-u>', '<leader>i' : 'gg0i',
    \ }]


"       *** WORD MOVEMENT *** 

" REMAP:  q w e
" TO:     u o p

" NOTE: shift-direction  : small jump, leave in normal
"       leader-direction : end jump  , go into insert mode

let s:mvword_a = [ "all", {
    \ 'p' : 'e', 'P' : 'E', '<leader>p' : 'ea', '<leader>P' : 'Ea',
    \ 'u' : 'b', 'U' : 'B', '<leader>u' : 'bi', '<leader>U' : 'Bi',
    \ }]

let s:mvword_n = [ [ "normal", "operation" ], {
    \ 'o' : 'w', 'O' : 'W', '<leader>o' : 'wi', '<leader>O' : 'Wi',
    \ }]

"let s:mvword_o = [ "operation", {
"    \ 'o' : ':execute "normal! viw" . v:count . "e"<cr>',
"    \ 'O' : ':execute "normal! viW" . v:count . "E"<cr>',
"    \ }]

" BUG
" In visual mode only the first time the first word should be 
" selected. Otherwise o and O might overwrite the viw|w. Also
" selecting a word with the cursor on the end selects the next 
" word by default otherwise
"
" However there is not autocmd for mode change (except insert)
"
" SOLUTION: manually track mode entering, e.g. (and same for leaving)
" nnoremap v : v :call trackmode()
" function trackmode()
"       if mode() == "v"                  "Make sure the mode is actually
"       entered
"           let g:MyTrackedMy=visual
"       endif
" endfunction
"
" Except there are mall ways to break out of visual mode
" Other solution might be to set a 'new enter state', checking
" the current mode before switching to visual
" If not visual, set new visual state to 1
"
" Proper solution: make function and save state of first command

let s:mvword_v = [ "visual", {
    \ 'o' : ':<silent> <c-u>execute "normal! `<viw`>" . ( v:count ? v:count : 1 ) . "e"<cr>',
    \ 'O' : ':<silent> <c-u>execute "normal! `<viW`>" . ( v:count ? v:count : 1 ) . "E"<cr>'
    \ }]


"        *** MOVEMENT SPLITS AND BUFFERS ***

"DEPRE: noremap <c-O> :%bp<cr>
" 1) Creation;  2) Movement between splits; 3) Movement between bufferts
let s:splits= [ "all", {
     \ '<c-n>' : ':vsp<cr>', '<c-m>' : ':sp<cr>',
     \ '<c-k>' : '<c-w><c-j>', '<c-i>' : '<c-w><c-k>', '<c-j>' : '<c-w><c-h>', '<c-l>' : '<c-w><c-l>',
     \ '<c-o>' : ':bn<cr>', '<c-u>' : ':bp<cr>', '<c-_>' : ':bd<cr>',
     \ }]


"       *** FOLDS ***

"1) open; 2) close
let s:folding= [ "all", {
    \ '<c-f>o' : 'zo', '<c-f>O' : 'zO', '<c-f>ao': 'zR', 
    \ '<c-f>c' : 'zc', '<c-f>C' : 'zC', '<c-f>ac': 'zM', 
    \ }]


"       *** EXIT AND SAVE ***

let s:exitNSave= [ "normal", {
    \ '<leader>sq' : ':wq<cr>', '<leader>s' : ':w<cr>' ,'<leader>q' : ':q!<cr>',
    \ }]


"       *** MODE SWITCHING ***

let s:normalModeSwitch = [ "normal", {
    \ '<leader>`' : 'i', '<leader>1' : 'v', '<leader>2' : '<c-v>', 
    \ }]

let s:visualModeSwitch = [ "visual", {
    \ '1<leader>' : '<esc>', '2<leader>' : '<esc>', '<leader>`' : 'I',
    \ }]

let s:insertModeSwitch = [ "insert", {
    \ '`<leader>' : '<esc>',
    \ }]

""       *** MISC ***
" 1) redo; 2) delete; 3) next; 4) whiteline; 5) highlight
let s:misc_n = [ "normal", {
            \ 'r' : 'u', 'R' : '<c-r>r',
            \ 'w' : 'i<bs><esc>l', 'e' : 'li<bs><esc>',
            \ '>' : 'n', '<' : 'N',
            \ '<leader>/' : ':set hlsearch! hlsearch?<cr>', 
            \ 's' : ':call moistfunc#Select()<cr>',
            \ '<C-b>' : ':call moistfunc#ToggleBin()<cr>',
            \ }]

"NOTE: same as <c-m>
"\ '<cr>' : 'i<cr><esc>',
"NOTE: same as <c-i>
"\ '<tab>q' : '@q', 

"let s:tab_n = [ "normal", {
"    \ '<tab>r' : 'v>gv<esc>', '<tab>R' : 'v<gv<esc>',
"    \ }]
"
"let s:tab_v = [ "visual", {
"    \ '<tab>r' : '>gv', '<tab>R' : '<gv',
"    \ }]


" Reverse direction
let s:misc_v = [ "visual", {
            \ 'r' : 'o'
            \ }]

" stty  -ixon (in .bash_profile)
"let s:misc_a = [ "all", {
"            \ '<c-t>s' : ':call moistfunc#ToggleScroll()<cr>',
"            \ }]

" Rebind 'i(nside)' (i) to 'w(ithin)'
" As i is used for up
let s:operationRemap = [ "operation", {
            \ 'w' : 'i',
            \ }]

let s:plugins_i = [ "insert", {
    \ '<C-T>' : '<C-R>=EasyCloseTag()<CR><C-R>=SetCursor()<CR>',
    \ }]


" 1) TAGBAR; 2) CTRLP; 3) YouCompleteMe; 4) Gutentags
let s:plugins_a = [ "all", {
    \ '<F8>'  : ':TagbarToggle<cr>',
    \ '<F2>'  : ':CtrlP',
    \ '<C-F>' : ':call FixItm', '<C-w>[' : ':call GoTom()', 
    \ '<C-G>' : ':call GetTags()',
    \ }]

" Yank, cut, copy, delete
let s:ycdp = [ "all", {
            \ 'c' : '"+y',
            \ 'd' : '"_d',
            \ 'x' : '"+d',
            \ }]

let s:ycd_n = [ "normal", {
            \  'cc' : '"+yy',
            \  'dd' : '"_dd',
            \  'xx' : '"+dd',
            \ }]

let s:paste_n = [ "normal", {
            \ 'v' : '"+p',
            \ }]

let s:paste_v = [ "visual", {
            \ 'v'  : '"_d"+p',
            \ }]

" Copy to file (for extra global copy registers)
" g@ calls operatorfunc
let s:yx_n_file = [ ["normal", "insert"], {
            \ '<C-c>' : ':set operatorfunc=CopyToFile<cr>g@', '<C-c>c' : ':call CopyLineToFile()<cr>',
            \ '<C-x>' : ':set operatorfunc=CutToFile<cr>g@',  'C-x>x'  : ':call CutLineToFile()<cr>',
            \ }]

let s:yx_v_file = [ "visual", { 
            \ '<C-c>' : ':<c-u>call CopyToFile(visualmode())<cr>',
            \ '<C-x>' : ':<c-u>call CutToFile(visualmode())<cr>',
            \ }]

let s:p_n_file = [ "normal", {
            \ '<c-v>' : ':call CopyFunctions_PasteFromFile()<cr>'
            \ }]

let s:p_v_file = [ "visual", {
            \ '<c-v>' : '"_d:call CopyFunctions_PasteFromFile()<cr>'
            \ }]

function! codemap#GetMap()
    return [ s:mvdirection, s:mvword_a, s:mvword_n,
                    \ s:mvword_v, s:splits, s:folding,
                    \ s:exitNSave, s:normalModeSwitch, s:visualModeSwitch,
                    \ s:insertModeSwitch, s:misc_n, s:misc_v,
                    \ s:misc_a, s:operationRemap, s:plugins_i,
                    \ s:plugins_a, s:ycdp, s:ycd_n,
                    \ s:paste_n, s:paste_v, s:yx_n_file,
                    \ s:yx_v_file, s:p_n_file, s:p_v_file ]
endfunction 

function codemap#LoadKeymap()
    call smartmap#NewMap("coding")
        call smartmap#AddMaps([ s:mvdirection, s:mvword_a, s:mvword_n,
                    \ s:mvword_v, s:splits, s:folding,
                    \ s:exitNSave, s:normalModeSwitch, s:visualModeSwitch,
                    \ s:insertModeSwitch, s:misc_n, s:misc_v,
                    \ s:misc_a, s:operationRemap, s:plugins_i,
                    \ s:plugins_a, s:ycdp, s:ycd_n,
                    \ s:paste_n, s:paste_v ])
        
                    "s:yx_n_file,
                    "\ s:yx_v_file, s:p_n_file, s:p_v_file ])
        let s:run = 1
    endif
endfunction

function! codemap#Enable()
    call smartmap#EnableKeyMap("coding")
endfunction
