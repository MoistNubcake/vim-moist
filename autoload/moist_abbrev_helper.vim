" API {{{1
" s:loadAbrrev {{{2
" The trailing space is need for a multiline iabbreviation

let b:abbrevs = {}
let s:UNLOAD = 1
let s:LOAD = 0

function! moist_abbrev_helper#setAbbrevs(abbrevs)
    let b:abbrevs = a:abbrevs
    command! MoistAbbrevLoad   :call s:loadAbbrev(s:LOAD)
    command! MoistAbbrevUnload :call s:loadAbbrev(s:UNLOAD)
endfunction


function! s:loadAbbrev(unload)
    let abbrevcmd = a:unload ? "iunabbrev" :  "iabbrev"

    if a:unload
        for [abbreviation, expansion] in items(b:abbrevs)
            silent! execute abbrevcmd . ' <buffer> ' . abbreviation
        endfor
    else
        for [abbreviation, expansion] in items(b:abbrevs)
            silent! execute abbrevcmd ' <buffer> ' . abbreviation . ' ' . expansion
        endfor
    endif

endfunction

function! moist_abbrev_helper#Eatchar()
    call getchar(0)
    return ''

endfunction
