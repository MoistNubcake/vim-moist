function! moist_purge#CleanRegColor(event)
    if  a:event["operator"] ==# 'y' && get(g:, "moist_vim_purge_colors", v:true)
        let regname = a:event["regname"]
        let regcontents = join(a:event["regcontents"])
        let options = a:event["regtype"]

        let pattern = '[.\{-}m'
        let newvalue = substitute(regcontents, pattern, '', 'g')
        if regcontents !=# newvalue
            call setreg(regname, newvalue, options)
            if &clipboard =~ 'unnamedplus'
                call setreg("+", newvalue, options)
            elseif &clipboard =~ 'unnamed'
                call setreg("*", newvalue, options)
            endif

            if get(g:, "moist_vim_purge_verbose", v:true)
                echo "purged colors while yanking (" . newvalue . ")"
            endif

            if get(g:, "moist_vim_debug", v:false)
                echom "[00]-vim-moist: called from  " . . expand("%:p")
            endif
        endif
    endif
endfunction
