if exists("moistfunc_loaded")
    finish
else
    let moistfunc_loaded = 1
endif

" Creates a list format of the current line or selection
"
" Example usage with ftplugin:
"
" function s:AsStringList(start, end, ...) range
"     if len(a:000) > 0
"         execute a:start .','. a:end . 'call moistfunc#AsList(a:1, 1, 0, "[", "]")'
"     else
"         execute a:start .','. a:end . 'call moistfunc#AsList(" ", 1, 0, "[", "]")'
"     endif
" endfunction
" command! -buffer -nargs=? -range AsStringList call s:AsStringList(<line1>, <line2>, <f-args>)
"
" @param - delim Item delimiter
" @param int quote if gt 1 quote items
" @param int linebreakAtEnd if gt 1 at the line break at the end of the line
" @param char starToken list start token
" @param char endToken list end token
function moistfunc#AsList(delim, quote, lineBreakAtEnd, startToken, endToken) range abort

    " Remove redundant white spaces
     execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#CleanSpaces(a:delim)'

    if a:quote
        " Quote words
        execute "silent " . a:firstline . ',' . (a:lastline) . 's/\([^' . a:delim . ']\+\)/"\1"/ge'
    endif

    " Replace delimiter and insert comma; remove redundant spaces
    execute "silent " . a:firstline . ',' . a:lastline . 's/' . a:delim . '/, /ge'

    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#AddLineBreak(a:lineBreakAtEnd)'
    execute "silent " . a:firstline . ',' . a:lastline . 'call s:CleanTokens()'
    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#AddAround(a:startToken, a:endToken)'
    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#CleanWhiteLines()'

endfunction

" Template function to create a dictionary
"
" e.g. hello<delim>world
" becomes: <startToken> "hello" : "world" <endToken>
" if isString1 and isString2 is set to 1 (Otherwise the <key> and <value> are
" not quoted
"
" Each language should call this function with its own parameters (in
" ftplugin)
"
" @param char delim Delimiter between <key,value> pair (default is whitespace)
" @param int isString1 if 1 quote <key>
" @param int isString2 if 1 quote <value>
" @param int lineBreakAtEnd if 1 line break at end (otherwise at the beginning
" of new line)
" @param char startToken Dictionary start token, e,g, {
" @param char endToken Dictionary end token, e.g. }
function moistfunc#AsDict(delim, isString1, isString2,
            \ lineBreakAtEnd, startToken, endToken) range abort

    let l:token1 = ""
    let l:token2 = ""
    let l:sepToken = " : "

    if a:isString1
        let l:token1 = "\""
    endif

    if a:isString2
        let l:token2 = "\""
    endif

    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#CleanSpaces(a:delim)'

    " Surround <key> and <value> with resp. token1 ('"') and token2 ('"')
    execute "silent " . a:firstline . ',' . a:lastline . 's/\([^ ' . a:delim . ']\+\)'
                \ . a:delim
                \ . '\([^ ' . a:delim . '\|$]\+\)'
                \ . '/' . l:token1 . '\1' . l:token1 . l:sepToken . l:token2 .'\2' . l:token2 . ', /ge'

    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#AddLineBreak(a:lineBreakAtEnd)'
    execute "silent " . a:firstline . ',' . a:lastline . 'call s:CleanTokens()'
    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#AddAround(a:startToken, a:endToken)'
    execute "silent " . a:firstline . ',' . a:lastline . 'call moistfunc#CleanWhiteLines()'
endfunction

" Delete redundant spaces around delimiter
" Especially useful if the delimiter is <space> resulting in 1 delimiter to
" parse subsequently with commands using regex
"
" @param char delim
function moistfunc#CleanSpaces(delim) range abort
    execute "silent " . a:firstline . ',' . a:lastline . 's/^\s*//'
    execute "silent " . a:firstline . ',' . a:lastline . 's/\s*$//'
    execute "silent " . a:firstline . ',' . a:lastline . 's/\s*'
                \ . a:delim . '\s*/' . a:delim . '/ge'
endfunction

" Create a line break
"
" @param int linebreakAtEnd if 1 break at the end of the line not the beginnen
" of the next line
function moistfunc#AddLineBreak(linebreakAtEnd) range abort
    " Add line break token and comma at end
    if a:linebreakAtEnd
        " Add line break at the end of the line except the last
        if a:lastline != a:firstline
            execute "silent " . a:firstline . ',' . (a:lastline - 1) . 's/\(., $\)/\1, \\/e'
        endif
    else
        " Add line break at the beginning of the line except the first
        if a:lastline != a:firstline
            " Add comma at the end of line except the last
            execute "silent " . a:firstline . ',' . (a:lastline - 1) . 's/\(.$\)/\1,/e'
            " Add the line break
            execute "silent " . (a:firstline + 1) . ',' . a:lastline  . 's/\(^.\)/\\ \1/e'
        endif
    endif
endfunction

" Surround group seperated by interval times <delim> with <startToken> and <endToken>
"
" @param char delim
" @param char startToken
" @param char endToken
" @param int interval
function moistfunc#AddGroup(delim, startToken, endToken, interval) range abort
    " Find (not-delim x times + delim) interval times
    execute "silent " . a:firstline . ',' . a:lastline . 's/\([^' . a:delim. ']\+\(' . a:delim . '\|$\)\)\{' . a:interval . '\}'
                \ . '/' . a:startToken . ' \0' . a:endToken . ' , ' . '/ge'
    " Replace the delimiter with '\s,\s'
    execute "silent " . a:firstline . ',' . a:lastline . 's/' . a:delim . '/ /ge'
    " Fix end of line (usually has no delimiter and has '\s,\s' added
    execute "silent " . a:firstline . ',' . a:lastline . 's/' . a:endToken . ' \+, \+$/ ' . a:endToken . '/ge'
endfunction

" Add <startToken> and <endToken> around selection/current line
"
" @param char startToken
" @param char endToken
function moistfunc#AddAround(startToken, endToken) range abort
    " Add [ at the start
    execute "silent " . a:firstline . 's/^/'. a:startToken .' /'
    " Add ] at the end
    execute "silent " . a:lastline  . 's/$/ '. a:endToken . '/'
endfunction

" Remove empty lines in selection/current line
function moistfunc#CleanWhiteLines() range abort
    " Remove white lines
    execute "silent " . a:firstline . ',' . a:lastline . 'g/^$/d'
endfunction

" Remove trailing spaces
function moistfunc#CleanTrailingSpaces()
    execute 'silent %s/\s*$//e'
endfunction

" Helper functions to clear trailing white spaces and commas
function s:CleanTokens() range abort
    " Clean up trailing/double tokens
    execute "silent " . a:firstline . ',' . a:lastline . 's/,\s*,$/,/e'
    execute "silent " . a:lastline . 's/\s*,\s*$//e'
endfunction

" Join multiples lines
function! moistfunc#Join() range abort
    if a:lastline != a:firstline
        execute 'silent' . a:firstline . ',' . (a:lastline-1) . 's/\n/ /'
    endif
endfunction

" Split items delimited by 'delim over multiple lines per 'interval'
" @param int interval amount of desired items per line
" @param string delim item delimiter
function! moistfunc#Split(interval, delim) range abort
    try
        execute 'silent' . a:firstline . ',' . a:lastline .  's/\([^' . a:delim . ']\+' . a:delim .'\)\{' . a:interval . '\}/\0\r/g'
    catch
        :
    endtry
endfunction

" Destribute items in range delimited by 'delim over multiple lines per
" 'interval' evenly over multiple lines
" @param int interval amount of desired items per line
" [@param char delim item delimiter]
function! moistfunc#DistributeOver(interval, ...) range abort
    if len(a:000) > 0
       let l:delim = a:1
    else
       let l:delim = " "
    endif

    execute a:firstline . ',' . a:lastline  . 'call moistfunc#Join()'
    execute a:firstline . 'call moistfunc#Split(a:interval, l:delim)'
endfunction

function! moistfunc#GetMatches(pattern) range abort

   " Save and clear unnamed register
   " NOTE: registers are not buffer bound so it does not matter where we clear
   " and reset it
   let reg_tmp=getreg('"')
   let regtype_tmp=getregtype('"')
   call setreg('"', '', '')

   try
       if a:firstline == a:lastline
           %yank
       else
           execute a:firstline . ',' . a:lastline  . "yank"
       endif

       new
       put!
       execute "%!egrep -o " . a:pattern
   finally
       call setreg('"', reg_tmp, regtype_tmp)
   endtry

endfunction

function! moistfunc#copyline2markm()
    let curpos_save = getcurpos()[1:2]
    t'm
    normal mm
    call cursor(curpos_save)
endfunction

function! moistfunc#copy2markm(type, ...)
    let curpos_save = getcurpos()[1:2]
    let t_reg_save = getreg('t')

    try
        " Get text selection
        if a:0
            echom "normal"
            silent exe 'normal! gv"ty'
        elseif a:type == 'line'
            echom "line"
            silent exe "normal! '[V']" . '"ty'
        else
            echom "visual"
            silent exe "normal! `[v`]" . '"ty'
        endif

        if get(g:, "copy2mark_linewise" , 1)
            normal! 'm
            put t
        else
            normal! `m
            normal! "tp
        endif
        normal mm
    finally
        call setreg('t', t_reg_save)
        call cursor(curpos_save)
    endtry

endfunction

function! moistfunc#toggleYCM()
    if exists("g:ycm_auto_trigger")
        let g:ycm_auto_trigger = get(g:, "ycm_auto_trigger", 0) ?  0 : 1
        echom "ycm auto trigger is " . (get(g:, "ycm_auto_trigger") ?  "ENABLED" : "DISABLED")
        " Sleep to show the message before returning to insert mode
        sleep 500m
    endif
endfunction

function! moistfunc#ultisnips_expand_jump()
     call UltiSnips#ExpandSnippetOrJump()
     return get(g:, "ulti_expand_or_jump_res", 0)
endfunction

function! moistfunc#RemoveTrailingSpace()
    let curpos_save = getcurpos()
    %s/\s\+$//ge
    call setpos('.', curpos_save)
endfunction

"function! moistfunc#Copy_and_jump_back(type, ...)
"    let curpos_save = getcurpos()[1:2]
"
"    if a:0
"        silent exe "normal! gvt`m"
"    elseif a:type == 'line'
"        silent exe "normal! '[V']t`m"
"    else
"        silent exe "normal! `[v`]t`m"
"    endif
"
"    normal mm
"    call cursor(curpos_save)
"endfunction
