autocmd BufNewFile,BufRead *.cdoc set ft=markdown
autocmd BufNewFile,BufRead *.ccs set ft=json
if get(g:, "moist_c_header", 1)
autocmd BufNewFile,BufRead *.h set ft=c
endif
autocmd BufNewFile,BufRead *.gdb set ft=gdb
autocmd BufNewFile,BufRead *.txt set modeline
