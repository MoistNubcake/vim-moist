" insecure
inoreabbrev <buffer> gets fgets

" Sadly the non keyword character has to be placed after the keyword
" characters. See :help abbrev
"
" Standard library include shortcuts
inoreabbrev <buffer> stdio# #include <stdio.h>
inoreabbrev <buffer> bool# #include <stdbool.h>
inoreabbrev <buffer> stdbool# #include <stdbool.h>
inoreabbrev <buffer> stdlib# #include <stdlib.h>
inoreabbrev <buffer> string# #include <string.h>
inoreabbrev <buffer> thread# #include <threads.h>
inoreabbrev <buffer> threads# #include <threads.h>
inoreabbrev <buffer> stdarg# #include <stdarg.h>
inoreabbrev <buffer> signal# #include <stdsignal.h>
inoreabbrev <buffer> errno# #include <errno.h>
inoreabbrev <buffer> error# #include <errno.h>

" Other libraries shortcut
inoreabbrev <buffer> unistd# #include <unistd.h>
