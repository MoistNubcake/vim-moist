"if exists("b:_moist_textop_cpp_map")
"    finish
"endif
"let b:_moist_textop_cpp_map = 1
"
"let g:moist_c_style_map = {
"            \ "PrevFuncOpen"  :   "<UP>", 
"            \ "NextFuncOpen"  :   "<DOWN>",
"            \ "NextFuncClose" :   "",
"            \ "PrevFuncClose" :   "",
"            \ "BlockDown"     :   "",
"            \ "BlockUp"       :   "",
"            \ "BockNext"      :   "" 
"            \ }

"map  <buffer> ]] <plug>cstyleMoveNextFuncOpen
"map  <buffer> [[ <plug>cstyleMovePrevFuncOpen
"map  <buffer> ][ <plug>cstyleMoveNextFuncClose
"map  <buffer> [] <plug>cstyleMovePrevFuncClose
"
"vmap <buffer> wf <plug>cstyleInsideFunction
"vmap <buffer> af <plug>cstyleAroundFunction
"nmap <buffer> wf <plug>cstyleInsideFunction
"nmap <buffer> af <plug>cstyleAroundFunction
"
"map  <buffer> bk <plug>cstyleMoveBlockDown
"map  <buffer> bi <plug>cstyleMoveBlockUp
"map  <buffer> bl <plug>cstyleMoveNextBlock
"map  <buffer> bj <plug>cstyleMovePrevBlock
