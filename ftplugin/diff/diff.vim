nmap s <plug>select
vmap s <plug>select

nmap d <plug>deselect
vmap d <plug>deselect

nmap c <plug>deselectall

nmap a <plug>applygitignore
vmap a <plug>applygitignore

unmap aa
unmap ao
unmap aO
