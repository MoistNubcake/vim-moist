" Header guard {{{1
if exists("b:_m4_iabbrev_loaded") || get(b:, "moist_abbrev_disable", 0)
   finish
else
    let b:_m4_iabbrev_loaded = 1
endif

let s:abbrevs = {
            \ '_[' : '@<:@',
            \ '_]' : '@:>@',
            \ '_$' : '@S\|@',
            \ '_#' : '@%:@',
            \ '_(' : '@{:@',
            \ '_)' : '@:}@'
            \ }

call moist_abbrev_helper#setAbbrevs(s:abbrevs)

if get(g:, "moist_load_abbrevs_default", 1)
    MoistAbbrevLoad
endif
