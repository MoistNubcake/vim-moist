" Header guard {{{1
if exists("b:_make_iabbrev_loaded") || get(b:, "moist_abbrev_disable", 0)
   finish 
else
    let b:_make_iabbrev_loaded = 1
endif

" TROUBLESHOOT READ FIRST {{{1
" NOTE: Do NOT remove trailing spaces for multiline abbreviations
" Example:
" iabbrev name<trailing space>
" \<CR>
" \<CR>
" \<CR>
" NOTE: Does not apply when the expansion is defined as a string (or a joined
" string ofcourse)

" Big abbreviations {{{1
let s:if=
\   '## Only outside recipes'
\ . '<CR>## see also ifneq, ifdef, ifndef'
\ . '<CR>## alternatively use the function $(if $(expandToNonEmpty), thentext, elsetext)'
\ . '<CR>## "textN" can be $(command)'
\ . '<CR>ifeq ("text1", "text2")'
\ . '<CR>:'
\ . '<CR>else ifeq("text1", "text2")'
\ . '<CR>:'
\ . '<CR>else'
\ . '<CR>:'
\ . '<CR>endif'

let s:function=
 \   '<CR>functionName = <body using ${1} ${2} ...>'
 \ . '<CR># Calling:'
 \ . '<CR># result = $(call functionName,arg1,arg2[,...])'

let s:for =
 \   '<CR> ##like in python'
 \ . '<CR> ##for var in list:'
 \ . '<CR> ##     newlist +=do_something(var)'
 \ . '<CR> ## EXAMPLE: '
 \ . '<CR> ## newlist := $(foreach dir, ${list_of_dirs}, $(wildcard ${dir}/*))'
 \ . '<CR> list := $(foreach var,list, text)'

" }}}1

" NOTE: for code makeup pusposes:
" put large abbreviation expansion in 'Big abbrevation' as a variableand 
" reference here
let s:abbrevs = { 
 \ '_new(list)'     : 'list := <value1> <value2> ...',
 \ '_del(list)'     : 'list := ',
 \ '_set(list)'     : 'TODO',
 \ '_add(list)'     : 'TODO',
 \ '_get(list)'     : 'TODO',
 \ '_len(list)'     : 'TODO',
 \ '_slice(list)'   : 'TODO',
 \ '_join(list)'    : 'TODO',
 \
 \ '_new(array)'    : 'list := <value1> <value2> ...',
 \ '_del(array)'    : 'list := ',
 \ '_set(array)'    : 'TODO ',
 \ '_add(array)'    : 'TODO ',
 \ '_get(array)'    : 'TODO ',
 \ '_len(array)'    : 'TODO',
 \ '_slice(array)'  : 'TODO',
 \ '_join(array)'   : 'TODO',
 \ '_for(array)'    : 'TODO',
 \ 
 \ '_len(string)'   : 'TODO',
 \ '_join(string)'  : 'TODO',
 \ '_charAt(string)': 'TODO',
 \ '_slice(string)' : 'TODO',
 \ '_printf'        : '@echo',
 \ 
 \ '_new(dict)'     : 'NONE',
 \ '_keys(dict)'    : 'NONE',
 \ '_values(dict)'  : 'NONE',
 \ '_add(dict)'     : 'NONE',
 \ '_haskey(dict)'  : 'NONE',
 \ '_del(dict)'     : 'NONE',
 \ '_set(dict)'     : 'NONE',
 \ '_get(dict)'     : 'NONE',
 \ '_for(dict)'     : 'NONE',
 \ 
 \ '_case'          : 'NONE, use _if',
 \ 'if'             : s:if,
 \ '_if'            : s:if,
 \ 'if(empty)'      : 'ifeq ($(command), )',
 \ 'if(not)'        : 'ifneq ("text1", "text2")',
 \ 'ifeq'           : 'ifeq ("text1", "text2")',
 \ 'else'           : 'ifeq ("text1", "text2")',
 \ 'fi'             : 'endif',
 \
 \ '#see'           : '_for',
 \ 'for'            : s:for,
 \ '_for'           : s:for,
 \ '_for(list)'     : '#see _for',
 \ '_for()'         : '#see _for',
 \ '_for(int)'      : '#see _for',
 \
 \ '_function'      : s:function,
 \ }

call moist_abbrev_helper#setAbbrevs(s:abbrevs)

if get(g:, "moist_load_abbrevs_default", 1)
    MoistAbbrevLoad
endif

