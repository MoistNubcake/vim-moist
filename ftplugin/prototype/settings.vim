augroup moistwarn
    au!
    au VimEnter * echoerr "File specific settings and abbrevations not set"
augroup END
