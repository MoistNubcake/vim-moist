" Header guard {{{1
if exists("b:_python_iabbrev_loaded") || get(b:, "moist_abbrev_disable", 0)
   finish
else
    au BufEnter test_*.py abbrev <buffer> bp breakpoint()<C-R>=moist_abbrev_helper#Eatchar()<CR>
    let b:_python_iabbrev_loaded = 1
endif

" TROUBLESHOOT READ FIRST {{{1
" NOTE: Do NOT remove trailing spaces for multiline abbreviations
" Example:
" iabbrev name<trailing space>
" \<CR>
" \<CR>
" \<CR>
" NOTE: Does not apply when the expansion is defined as a string (or a joined
" string ofcourse)

" Big abbreviations {{{1
" Not directly in the s:abbrev dictionary for makeup purposes
let s:function =
  \   '<CR>"""'
  \ . '<CR>NOTE ON FUNCTION DEFINITION'
  \ . '<CR>Default/Optional param        : param1 = <value>, ...'
  \ . '<CR>Variable length param         : param1, ... *var_length_list'
  \ . '<CR>'
  \ . '<CR>NOTE ON FUNCTION CALL'
  \ . '<CR>Argument (emulate*) pass by reference!            *see C++ for true pass by reference'
  \ . '<CR>Keyword [out of order] call   : func(param2 = <value>, param1 = <value>)'
  \ . '<CR>"""'
  \ . '<CR>def <name>(<params>):'
  \ . '<CR>"""'
  \ . '<CR>synopsis'
  \ . '<CR>"""'
  \ . '<CR>pass'
  \ . '<CR>#return <var>'

let s:overload =
 \   '# No pseudo types allowed (Sequence)'
 \ . '<CR># Use python3.7 or register(type)'
 \ . '<CR>from functools import singledispatch'
 \ . '<CR># Fallback function'
 \ . '<CR>@singledispatch'
 \ . '<CR>def foo([self,] arg):'
 \ . '<CR>pass'
 \ . '<CR>'
 \ . '<CR>@foo.register'
 \ . '<CR>def _([self,] arg: <type>):'
 \ . '<CR>pass'


let s:switch =
  \   '<CR># NO BUILTIN, USE IFELSE OR DICTIONARY:'
  \ . '<CR># More memory intensive (because uses a dictionary) but faster than ifelse'
  \ . '<CR>def func1: '
  \ . '<CR>pass'
  \ . '<CR>'
  \ . '<CR>def func2:'
  \ . '<CR>pass'
  \ . '<CR>'
  \ . '<CR>def defaultFunc:'
  \ . '<CR>pass'
  \ . '<CR>'
  \ . '<CR>def switch_func(param):'
  \ . '<CR>" Create dictionary'
  \ . '<CR>switcher = {'
  \ . "<CR>'option1': func1,"
  \ . "<CR>'option2': func2,"
  \ . '<CR>}'
  \ . '<CR>" Call function or a lambda function called default'
  \ . '<CR>switcher.get(param, defaultFunc)()'
" }}}1

" NOTE: for code makeup pusposes:
" put large abbreviation expansion in 'Big abbrevation' as a variableand
" reference here
let s:abbrevs = {
  \   '_function'       : s:function,
  \   '_new(list)'      : 'list = [<value1>, <value2>, ...]',
  \   '_del(list)'      : 'del list[index]',
  \   '_set(list)'      : 'list[i] =',
  \   '_add(list)'      : 'list.append(obj)',
  \   '_get(list)'      : 'list[i]',
  \   '_len(list)'      : 'len(list)',
  \   '_slice(list)'    : 'list[start:end]',
  \   '_join(list)'     : 'list1 + list2',
  \
  \   '_new(array)'     : 'array = [<value1>, <value2>, ...]',
  \   '_del(array)'     : 'del array[index]',
  \   '_set(array)'     : 'array[i] =',
  \   '_add(array)'     : 'array.append()',
  \   '_get(array)'     : 'array[i]',
  \   '_len(array)'     : 'len(array)',
  \   '_slice(array)'   : 'array[start:end]',
  \   '_join(array)'    : 'array1 + array2',
  \
  \   '_len(string)'    : 'len(string)',
  \   '_new(string)'    : 'var = "string"',
  \   '_join(string)'   : 'string1 + string2',
  \   '_charAt(string)' : 'string[index]',
  \   '_slice(string)'  : 'string[start:end]',
  \   '_set(string)'    : 'new = [:index] + <char> + [index+1:]',
  \
  \   '_printf'         : '<string>.format(var, ...) # With {} in string as placeholder',
  \   '_new(dict)'      : "dict = {'key' : value, ...}",
  \   '_keys(dict)'     : 'dict.keys()',
  \   '_values(dict)'   : 'dict.values()',
  \   '_add(dict)'      : "dict['key'] = value # Or dict.update(dict2)",
  \   '_haskey(dict)'   : '<key> in <dict>',
  \   '_del(dict)'      : "del dict['key']",
  \   '_set(dict)'      : "dict['key'] =",
  \   '_get(dict)'      : "dict['key'] # Or dict.get(key, default)",
  \
  \   'function'        : 'def',
  \   '_overload'       : s:overload,
  \   'catch'           : 'except(<exception>):',
  \
  \   'switch'          :  s:switch,
  \   '_if'             : '<CR>if <condition>:<CR>pass<CR>',
  \   'if('             : 'if',
  \   'elseif'          : 'elif',
  \   'elseif('         : 'elif',
  \   'elif('           : 'elif',
  \   'endif'           : '# Just unindent',
  \   '_case'           : '# See _switch',
  \   '_ternary'        : '#example: var=1 if 2==2 else 2'
  \                     . '<CR><expression> if <condition> else <expression>',
  \
  \   '&&'              : 'and',
  \   '\|\|'            : 'or',
  \
  \   '_while'          : '<CR>while <cond>:<CR>pass<CR>',
  \   '_for(list)'      : '<CR>for item in list; do <CR>pass<CR>done',
  \   '_for'            : '<CR>for i in range(start, end):<CR>pass<CR>done',
  \   '_for()'          : '<CR>for i in range(start, end):<CR>pass<CR>done',
  \   '_for(int)'       : 'for i in range(start, end):<CR>pass<CR>done',
  \   '_for(array)'     :  '<CR>for item in array; do<CR>:<CR>done',
  \   '_for(dict)'      : "<CR># or a normal tuple or a list, or just 'key, value'<CR>for (key, value) in dict.items:<CR>:<CR>done",
  \
  \   '_version'        : 'version = int("".join(map(str, sys.version_info[0:2])))',
  \   '_ls'             : 'glob.glob("<globpattern>", recursive=True)<CR> # See also os.path.expandvars() for tilde expansion<CR> # See also os.path.isfile\|isdir(<path>)',
  \   '_ifname'         : "if __name__ == '__main__':",
  \   '#include'        : "import",
  \   '_include'        : "import <package> #from <package> import <functionname/variable/etc.>",
  \   '_shebang'        : "#!/usr/bin/env python",
  \
  \   'static'          : '#NONE: simulate in a class',
  \   '_class'          : 'class <name>():<CR><CR>"""Doc"""<CR><CR>def __init__(self, initvalue):<CR>self.var=initvalue',
  \   '_tuple'          : 'a=(1,2,3)',
  \
  \   '_type(tuple)'    : 'from typing import Tuple<CR>MyTuple=Tuple[<type>, <type>, ...]<CR>#def foo(arg: MyTuple):<CR>pass',
  \   '_type(2Dlist)'   : 'from typing import List Sequence<CR>def foo(Sequence[List[List]])',
  \
  \   '_linebreak'      : '\ # (or wrap in parentheses - mind the newline new indentation)',
  \   '_this'           : 'global',
  \ }

call moist_abbrev_helper#setAbbrevs(s:abbrevs)

if get(g:, "moist_load_abbrevs_default", 1)
    MoistAbbrevLoad
endif
