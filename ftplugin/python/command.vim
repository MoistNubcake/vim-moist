if exists("g:loaded_moist_python")
    finish
endif
let g:loaded_moist_python = 1

function! s:AsStringList(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsList(a:1, 1, 0, "[", "]")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsList(" ", 1, 0, "[", "]")'
    endif
endfunction

function! s:AsIntList(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsList(a:1, 0, 0, "[", "]")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsList(" ", 0, 0, "[", "]")'
    endif
endfunction

function! s:AsDict(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsDict(a:1, 0, 1, 0, "{", "}")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsDict(" ", 0, 1, 0, "{", "}")'
    endif
endfunction


command! -buffer -nargs=? -range AsIntList call s:AsIntList(<line1>, <line2>, <f-args>)
command! -buffer -nargs=? -range AsIntList call s:AsIntList(<line1>, <line2>, <f-args>)
command! -buffer -nargs=? -range AsStringList <line1>,<line2>call s:AsStringList(<line1>, <line2>, <f-args>)
