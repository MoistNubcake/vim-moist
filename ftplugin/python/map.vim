" Indent movement
map bl <plug>PyBlockNext
map bj <plug>PyBlockPrev
map bk <plug>PyBlockDown
map bi <plug>PyBlockUp

vmap wB <plug>inBlock
vmap aB <plug>aroundBlock
omap aB <plug>aroundBlock
omap wB <plug>inBlock

vmap wb <plug>inBlock
vmap ab <plug>aroundBlock
omap wb <plug>inBlock
omap ab <plug>aroundBlock

map ]} <plug>PyEndBlock
map [{ <plug>PyStartBlock
