set foldmethod=indent
set textwidth=79

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let b:syntastic_mode = "passive"
let g:syntastic_python_checkers = ["mypy", "flake8"]
"let g:syntastic_check_on_open = 0
"let g:syntastic_check_on_wq = 0
let g:syntastic_always_populate_loc_list = 1

:let g:python_highlight_all = 1
