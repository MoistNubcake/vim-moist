" Header guard {{{1
if exists("b:_sh_iabbrev_loaded") || get(b:, "moist_abbrev_disable", 0)
   finish 
else
    let b:_sh_iabbrev_loaded = 1
endif

" TROUBLESHOOT READ FIRST {{{1
" NOTE: Do NOT remove trailing spaces for multiline abbreviations
" Example:
" iabbrev name<trailing space>
" \<CR>
" \<CR>
" \<CR>
" NOTE: Does not apply when the expansion is defined as a string (or a joined
" string ofcourse)

" Big abbreviations {{{1
" Not directly in the s:abbrev dictionary for makeup purposes

let s:argparse = 
 \    "<CR>argparse() {"
 \  . "<CR>"
 \  . "<CR># 1 trailing colon signifies required option, 2 trailing colons signifies"
 \  . "<CR># optional argument."
 \  . "<CR># Long arguments comma separated"
 \  . "<CR>TEMP=$(getopt -o \'@SHORTOPTIONS@\' --long \'@LONG_OPTIONS@\' -n \'@PROGRAM_NAME@\' -- \"$@\")"
 \  . "<CR>"
 \  . "<CR>if [ $? -ne 0 ]; then"
 \  . "<CR>echo 'Terminating...' >&2"
 \  . "<CR>exit 1"
 \  . "<CR>fi"
 \  . "<CR>"
 \  . "<CR># Note the quotes around \"$TEMP\": they are essential!"
 \  . "<CR># 'eval' preserves whitespaces when arguments are quoted"
 \  . "<CR>eval set -- \"$TEMP\""
 \  . "<CR>unset TEMP"
 \  . "<CR>"
 \  . "<CR># PARSE_OPTIONS"
 \  . "<CR># Example -f::"
 \  . "<CR># \'-f\' \\|'--foo\')"
 \  . "<CR># case \"$2\" in"
 \  . "<CR># \'\')"
 \  . "<CR># echo 'Option foo, no argument'"
 \  . "<CR># ;;"
 \  . "<CR># *)"
 \  . "<CR># echo \"Option foo, argument \'$2\'\""
 \  . "<CR># ;;"
 \  . "<CR># esac"
 \  . "<CR># shift 2"
 \  . "<CR># continue"
 \  . "<CR># ;;"
 \  . "<CR>while true; do"
 \  . "<CR>case \"$1\" in"
 \  . "<CR>'--')"
 \  . "<CR>shift"
 \  . "<CR>break"
 \  . "<CR>;;"
 \  . "<CR>*)"
 \  . "<CR>echo 'Internal error!' >&2"
 \  . "<CR>exit 1"
 \  . "<CR>;;"
 \  . "<CR>esac"
 \  . "<CR>done"
 \  . "<CR>"
 \  . "<CR>for arg; do"
 \  . "<CR># PARSE_REMAINING_ARGS"
 \  . "<CR>:"
 \  . "<CR>done"
 \  . "<CR>"
 \  . "<CR>}"
 \  . "<CR>"
 \  . "<CR>argparse \"$@\""

let s:forl = 
 \   "<CR>IFS=' '"
 \ . '<CR>for item in ${array[@]}; do'
 \ . '<CR>:'
 \ . '<CR>done'

let s:function =
 \   '<CR># Or function name{<body>}'
 \ . "<CR># NOTE: enclosing the body in '(' and ')' starts a subshell"
 \ . '<CR># NOTE: args are accessed by $1, $2 ... $n. All args: $@. len(args): $#'
 \ . '<CR># NOTE: args are NOT names between the parentheses (parentheses only)'
 \ . '<CR># indicated a new function start)'
 \ . '<CR>name(){'
 \ . '<CR>:'
 \ . '<CR>}'

let s:ford =
 \   "<CR>IFS=' '"
 \ . '<CR>for key in ${!dict[@]}; do'
 \ . '<CR>:'
 \ . '<CR>done'

let s:fori =
 \   '<CR>for((i=0; i<$var; i++)); do'
 \ . '<CR>:'
 \ . '<CR>done'

let s:case = 
 \   '<CR> #NOTE: regex uses glob patterns, not BRE/ERE behaviour'
 \ . '<CR>case "$var" in'
 \ . '<CR><glob>):'
 \ . '<CR>break'
 \ . '<CR>;;'
 \ . '<CR>esac'

let s:if = 
\   '<CR>if test <condition>; then'
\ . '<CR>:'
\ . '<CR>fi'

let s:while = 
 \   '<CR>while <cond>; do'
 \ . '<CR>:'
 \ . '<CR>done'
"}}}1

" NOTE: for code makeup pusposes:
" put large abbreviation expansion in 'Big abbrevation' as a variableand 
" reference here
let s:abbrevs = { 
 \  '_seq'              : '{start..end[..interval]}',
 \  '_arith'            : '$((<expr>)) ',
 \  
 \  '_copy(array)'      : 'mycopy=("${originalArray[@]}")',
 \  '_new(array)'       : '[declare -a] array=(one two three)',
 \  '_set(array)'       : 'array[i]=',
 \  '_get(array)'       : '${array[i]}',
 \  '_len(array)'       : '"${#array[@]}',
 \  '_slice(array)'     : '"${array[@]:start:end}"',
 \  '_join(array)'      : 'array1=("${array2[@]}")',
 \  '_add(array)'       : 'array+=(item)',
 \  
 \  '_len(string)'      : 'strlen(string)',
 \  '_join(string)'     : 'new_string = string1 . string2',
 \  '_charAt(string)'   : 'strpart(string, index, index)',
 \  '_slice(string)'    : 'strpart(string, start[, end])',
 \  
 \  '_new(dict)'        : 'declare -A dict=([key]=value [key2]=value)',
 \  '_keys(dict)'       : '"${!dict[@]}"',
 \  '_add(dict)'        : 'dict[key]=',
 \  '_get(dict)'        : '"${dict[key]}"',
 \  
 \  '_if'               :  s:if,
 \  'if('               : 'if',
 \  'elseif'            : 'elif',
 \  'elseif('           : 'elif',
 \  'elif('             : 'elif',
 \  'endif'             : 'fi',
 \  
 \  '_for(array)'       : s:forl,
 \  '_for(list)'        : s:forl,
 \  '_function'         : s:function,
 \  '_for(dict)'        : s:ford,
 \  '_for(int)'         : s:fori,
 \  '_case'             : s:case,
 \  '_while'            : s:while,
 \ 
 \  '_printf'           : '\<CR>printf "%s\n" "$param"',
 \  '_argparse'         : s:argparse,
\ }

call moist_abbrev_helper#setAbbrevs(s:abbrevs)

if get(g:, "moist_load_abbrevs_default", 1)
    MoistAbbrevLoad
endif
