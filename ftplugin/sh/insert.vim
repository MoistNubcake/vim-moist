let s:currentPath=expand("<sfile>:p:h")
let g:PRE_DEF_STRUCT=split(system("ls " . s:currentPath . "/structs/"))

function! RequestInsertFT()

    " Request file
    let l:counter=0
	echo "Select from: "
	for l:struct in g:PRE_DEF_STRUCT	
		echo l:struct . " (" . l:counter . ")"
		let l:counter=l:counter + 1
	endfor

	call inputsave()
	let l:request = input("")
	call inputrestore()
	echo "\n"

	if l:request > 0 || l:request <= len(g:PRE_DEF_STRUCT)
		echo g:PRE_DEF_STRUCT[l:request] . " selected"	
	else
		echo "Invalid request: " . l:request
	endif

	let l:file=g:PRE_DEF_STRUCT[l:request]
	:call BufferFile(l:file)
	
endfunction

"@OVERWRITE
function! BufferFile(file)

    " Remember current fold method
    let l:prefFM=&foldmethod 
    set foldmethod=manual 

    " Insert file
    let l:file=s:currentPath . "/structs/" . a:file
	if filereadable(l:file)
		execute ":read " . l:file
	else
		echo l:file . " does not exist"	
	endif
   
    " Indent
    let l:command="wc -l <" .  l:file
	let l:fileLength=system(l:command)

	execute "normal! v" . l:fileLength . 'j'
	execute "normal! ="
    
    " Reset foldmethod
    let &foldmethod=l:prefFM
endfunction
