" FILE TYPE
" set ft?

" GENERAL COLOR COMMANDS
" :color *current color scheme
" :hi    *current color names

" SYNTAX MATCHING
" syn <keyword|match|region> <color_group> <word|regex> [extra_options]

if exists("b:current_syntax")
    finish
endif

let b:current_syntax="summary"

" DEBUG: somehow this is set off somewhere 
" syn needs this to be on or it wont register the syntax matches

" NOTE: if the color_group does not exists it does not register. Does not
" throw an error

syntax on 
syn match Head "\v\*\*\*+[A-Z ]*\*\*\*+"
syn match Captial "\v[A-Z0-9]+[A-Z0-9]+[,|:]*"
"   -|--|*word
syn match List1 "\v[\t|    ]+-[a-zA-Z0-9]+"
syn match List1 "\v[\t| ]+-.+-[\t| ]+ "
syn match List2 "\v[\t|    ]+--[a-zA-Z0-9]+"
syn match List2 "\v[\t| ]+--.+--[\t| ]"
syn match List3 "\v[\t|    ]+\*[a-zA-Z0-9]+"
syn match List3 "\v[\t|    ]+\*.+\*"
" num) or num]
syn match Enum "\v[1-9]+[\)|\]] "

syn match bg1 "\v\=.*\="
syn match bg2 "\v\+.*\+"

hi Head ctermfg=131 ctermfg=66
hi Captial ctermfg=208
hi List1 ctermfg=10
hi List2 ctermfg=9
hi List3 ctermfg=12
hi Enum ctermfg=3
hi bg1 ctermbg=242
hi bg2 ctermbg=238
