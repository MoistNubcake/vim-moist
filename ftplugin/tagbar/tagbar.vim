" There is no option to set the keymap in tagbar so we load our own at
" BufEnter (once)

autocmd  BufEnter <buffer> ++once noremap <buffer> i k
