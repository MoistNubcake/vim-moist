" Manpage settings {{{1
" Disables statusbar like vim-airline
setlocal laststatus=0
" Do not show mode message on cmdline
setlocal noshowmode
setlocal nonumber
setlocal norelativenumber
setlocal ruler
setlocal noshowcmd
setlocal noswapfile
setlocal nomodifiable
setlocal tabstop=8
" Do not unload after abandoning the buffer, needed to pop man pages
setlocal hidden
setlocal buftype=nofile
setlocal buflisted

" colorscheme ron

" #airline is the event
" Checks if the plugin airline exists
if exists("#airline")
    " Disable airline
    AirlineToggle
endif

if exists(":AnsiEsc")
    augroup AnsiEsc
        autocmd! VimEnter <buffer> :AnsiEsc
    augroup END
endif

" Hack to disable ycm when reading from stdin and ft is set to view
" youcompleteme chokes on reading from stdin ('the ycmd server SHUTDOWN')
augroup youcompletemeStart
    autocmd!
augroup END

augroup cleanReg
    autocmd! TextYankPost * :call moist_purge#CleanRegColor(v:event)
augroup END
