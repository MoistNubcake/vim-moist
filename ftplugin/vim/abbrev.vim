" Header guard {{{1
if exists("b:_python_iabbrev_loaded") || get(b:, "moist_abbrev_disable", 0)
   finish 
else
    let b:_python_iabbrev_loaded = 1
endif

" TROUBLESHOOT READ FIRST {{{1
" NOTE: Do NOT remove trailing spaces for multiline abbreviations
" Example:
" iabbrev name<trailing space>
" \<CR>
" \<CR>
" \<CR>
" NOTE: Does not apply when the expansion is defined as a string (or a joined
" string ofcourse)

" Big abbreviations {{{1
" Not directly in the s:abbrev dictionary for makeup purposes
let s:function =  
 \   '<CR>function! <name>(a:param, a:param2 ...)'
 \ . '<CR>" access parameters e.g.:'
 \ . '<CR>" echo a:param a:param2'
 \ . '<CR>" a:1 ... a:9: va_arg, arguments set to 0 if not given during call'
 \ . '<CR>:'
 \ . '<CR>endfunction'

let s:try = 
 \   '<CR>try'
 \ . '<CR>:'
 \ . '<CR>catch * " messages pattern from thow, * matching all exceptions'
 \ . '<CR>: "echo v:exception'
 \ . '<CR>endtry'
 
let s:forl =
 \   '<CR>for item in list; do'
 \ . '<CR>pass'
 \ . '<CR>done'

let s:ford =
 \   '<CR>for [key, value] in items(dict)'
 \ . '<CR>:'
 \ . '<CR>endfor'

let s:fori =
 \   '<CR>"NO BUILTIN, USE WHILE'
 \ . '<CR>while i < index'
 \ . '<CR>:'
 \ . '<CR>i++'
 \ . '<CR>endwile'

let s:switch = 
 \   '<CR># NO BUILTIN, USE IFELSE OR DICTIONARY:'
 \ . '<CR># More memory intensive (because uses a dictionary) but faster than ifelse'
 \ . '<CR>function func1:'
 \ . '<CR>:'
 \ . '<CR>endfunction'
 \ . '<CR>'
 \ . '<CR>function func2:'
 \ . '<CR>:'
 \ . '<CR>endfunction'
 \ . '<CR>'
 \ . '<CR>function defaultFunc:'
 \ . '<CR>:'
 \ . '<CR>endfunction'
 \ . '<CR>'
 \ . '<CR>function Switch_func(param):'
 \ . '<CR>" Create dictionary'
 \ . '<CR>switcher = {'
 \ . "<CR>'option1': 'func1',"
 \ . "<CR>'option2': 'func2"
 \ . '<CR>}'
 \ . '<CR>" Get and execute the function'
 \ . "<CR>execute 'call ' . get(switcher, param, defaultFunc) . '()"
 \ . '<CR>endfunction'

let s:if =
 \   '<CR>if <condition>'
 \ . '<CR>:'
 \ . '<CR>endif'

let s:while=
 \   '<CR>while <cond>'
 \ . '<CR>:'
 \ . '<CR>endwhile'

let s:try =
 \   '<CR>try'
 \ . '<CR>:'
 \ . '<CR>catch <message_pattern>'
 \ . '<CR>: "echo v:exception'
 \ . '<CR>endtry'

" NOTE: for code makeup pusposes:
" put large abbreviation expansion in 'Big abbrevation' as a variableand 
" reference here
let s:abbrevs = { 
 \ '_new(list)'     : 'let list = [<value1>, <value2>, ...]',
 \ '_del(list)'     : '"NO BUILTIN',
 \ '_set(list)'     : 'list[i] =',
 \ '_add(list)'     : 'call add(list, value)',
 \ '_has(list)'     : 'call index(list, value)',
 \ '_get(list)'     : "get(list, index, 'default') \"or list[i] (unsafe)",
 \ '_len(list)'     : 'call len(list)',
 \ '_slice(list)'   : 'list[start:end]',
 \ '_join(list)'    : 'list1 + list2',
 \
 \ '_new(array)'    : 'let array = [<value1>, <value2>, ...]',
 \ '_del(array)'    : '"NO BUILTIN',
 \ '_set(array)'    : 'array[i] =',
 \ '_add(array)'    : 'call add(array, value)',
 \ '_has(array)'    : 'call index(array, value)',
 \ '_get(array)'    : "call get(array, index, 'default') \"or array[i] (unsafe)",
 \ '_len(array)'    : 'call len(array) ',
 \ '_slice(array)'  : 'array[start:end]',
 \ '_join(array)'   : 'array1 + array2',
 \
 \ '_len(string)'   : 'len(string)',
 \ '_join(string)'  : 'string1 . string2 " Uses . operator because vim supports string coersion',
 \ '_charAt(string)': 'string[index]',
 \ '_slice(string)' : 'string[start:end]',
 \ '_substring(string)' : 'string[start:end]',
 \ '_printf'        : '"NO BUILTIN',
 \
 \ '_new(dict)'     : "let dict = {'key' : value, ...}",
 \ '_keys(dict)'    : 'call keys(dict)',
 \ '_values(dict)'  : 'call values(dict)',
 \ '_items(dict)'   : 'let [key, value] = items(dict)',
 \ '_add(dict)'     : "dict.key = value \" Or dict = {'key' : value}",
 \ '_haskey(dict)'  : "has_key(dict, 'key')",
 \ '_del(dict)'     : "call remove(dict, 'key') \" or unlet dict.key (unsafe)",
 \ '_set(dict)'     : "dict.key = value # Or dict = {'key' : value}",
 \ '_get(dict)'     : "call get(dict, 'key', default) \"or dict.key  (unsafe)",
 \
 \ '_true'          : 'v:true " or 1',
 \ '_false'         : 'v:false " or 0',
 \
 \ 'if('            : 'if',
 \ 'elif'           : 'elseif',
 \ 'elif('          : 'elseif',
 \ 'elseif('        : 'elseif',
 \ 'fi'             : 'endif ',
 \
 \ '/**/'           : '"<cr><cr>',
 \ '//'             : '"',
 \ '#'              : '"',
 \ 
 \ 'global'         : 'g:var',
 \ 'local'          : 'l:var',
 \ '_global'        : 'g:var',
 \ '_local'         : 'l:var',
 \ '_buffer'        : 'b:var',
 \ '_param'         : 'a:param',
 \ '_arg'           : 'a:param',
 \
 \ 'def'            : 'function',
 \ '_function'      : s:function,
 \
 \ '_for'           : '" use _for(int)',
 \ '_for()'         : '" use _for(int)',
 \ '_for(array)'    : s:forl,
 \ '_for(dict)'     : s:ford,
 \ '_for(int)'      : s:fori,
 \
 \ '_case'          : '# See _switch',
 \ '_switch'        : s:switch,
 \ '_if'            : s:if,
 \ '_while'         : s:while,
 \
 \ '_try'           : s:try,
 \ '_catch'         : 'catch <message_pattern>',
 \ 'except'         : 'catch <message_pattern>',
 \ }

call moist_abbrev_helper#setAbbrevs(s:abbrevs)

if get(g:, "moist_load_abbrevs_default", 1)
    MoistAbbrevLoad
endif
