" LOADGUARD {{{9
if exists("b:vim_command_loaded")
    finish
endif
let b:vim_command_loaded = 1
" }}}9

" List/Dictionary implementation {{{1
" List/Dict functions implementation {{{3
function! s:AsStringList(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsList(a:1, 1, 0, "[", "]")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsList(" ", 1, 0, "[", "]")'
    endif
endfunction

function! s:AsIntList(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsList(a:1, 0, 0, "[", "]")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsList(" ", 0, 0, "[", "]")'
    endif
endfunction

function! s:AsDict(start, end, ...) abort
    if len(a:000) > 0
        execute a:start .','. a:end . 'call moistfunc#AsDict(a:1, 0, 1, 0, "{", "}")'
    else
        execute a:start .','. a:end . 'call moistfunc#AsDict(" ", 0, 1, 0, "{", "}")'
    endif
endfunction

" Commands {{{2
command! -buffer -nargs=? -range AsIntList call s:AsIntList(<line1>, <line2>, <f-args>)
command! -buffer -nargs=? -range AsStringList call s:AsStringList(<line1>, <line2>, <f-args>)
command! -buffer -nargs=? -range AsDict call s:AsDict(<line1>, <line2>, <f-args>)
