" LOADGUARD {{{9
if exists("b:vim_settings_loaded")
    finish
endif
let b:vim_settings_loaded=1

" Tabs {{{2
" expand tabs into spaces
set expandtab 
" tabstop: how tabs are represented (keep at 8)
set tabstop=8 
" softtabstop: how inserted tabs are translated
set softtabstop=4 
" shiftwidth: how autoindentation is translated
set shiftwidth=4 

" Folding {{{2
" No builtin syntax folding for vimscript
set foldmethod=marker
set foldenable 
" Show fold depth on the side
set foldcolumn=1

set foldlevel=1
set modeline
