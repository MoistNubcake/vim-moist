command! MoistRemoveTrailingSpaces call moistfunc#RemoveTrailingSpace()
command! RemoveTrailingSpaces call moistfunc#RemoveTrailingSpace()

command! -nargs=+ -range MoistDistributeOver <line1>, <line2>call moistfunc#DistributeOver(<f-args>)
command! -nargs=+ -range DistributeOver <line1>, <line2>call moistfunc#DistributeOver(<f-args>)

command! -nargs=1 -range MoistGetRegex <line1>, <line2>call moistfunc#GetMatches(<f-args>)
command! Scroll execute "set so=" . (&so > 0 ? 0 : 999)

command! GetSyntax :echo "syn <" . synIDattr(synID(line("."),col("."),1),"name") . '> transp_syn <'
            \ . synIDattr(synID(line("."),col("."),0),"name")
            \ . "> ultimate_hl<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">\<CR>"
