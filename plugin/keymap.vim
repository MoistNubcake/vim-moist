" MOVEMENT {{{1
" character/line movement {{{2

" REMAP: h  j  l  k
" TO:    j  k  l  i

" NOTE: shift-direction         : small jump
"       leader-direction        : end jump
"       shift leader-direction  : end jump, insert mode


noremap <special> j             h
noremap <special> <leader>j     hi
noremap <special> J             ^
noremap <special> <leader>J     I
noremap <special> <leader>0     0i

noremap <special> l             l
noremap <special> <leader>l     a
noremap <special> L             $
noremap <special> <leader>L     A
noremap <special> <leader>$     A

noremap <special> k             j
noremap <special> K             G$
noremap <special> <leader>K     G$a

noremap <special> i             k
noremap <special> I             gg
noremap <special> <leader>I     ggi

noremap Q <c-u>
noremap E <c-d>

" word movement {{{2

" REMAP:  b ge w e
" TO:     y u  o p

" NOTE: shift-direction  : small jump, leave in normal
"       leader-direction : end jump, go into insert mode

"TODO remap around and inside words as objects --> ciw : cwo

noremap <special> y         b
noremap <special> Y         B
noremap <special> <leader>y bi
noremap <special> <leader>Y Bi

noremap <special> u         ge
noremap <special> U         gE
noremap <special> <leader>u gea
noremap <special> <leader>U gEa

noremap <special> o         w
noremap <special> O         W
noremap <special> <leader>o wi
noremap <special> <leader>O Wi

noremap <special> p         e
noremap <special> P         E
noremap <special> <leader>p ea
noremap <special> <leader>P Ea

" TEXT OBJECTS {{{1
onoremap wo iw
onoremap wO iW
onoremap ao aw
onoremap aO aW

vnoremap wo iw
vnoremap wO iW
vnoremap ao aw
vnoremap aO aW

" misc {{{2
" WINDOWS AND BUFFERS {{{1
" buffer {{{2
noremap <special> <c-o> :bn<cr>
noremap <special> <c-u> :bp<cr>
noremap <special> <c-_> :bd<cr>

" tabs {{{2
"noremap <special> <leader>tt  :tabnew<cr>
"noremap <special> <leader>to  :tabnext<cr>
"noremap <special> <leader>tu  :tabprevious<cr>
noremap <special> <c-w>t  :vnew<cr>
noremap <special> <c-w><c-t>  :vnew<cr>
noremap <special> <c-w>>  :vnew<cr>
noremap <special> <c-w><  :vnew<cr>

" windows {{{2
" new {{{3
"noremap <special> <leader>sh :sp<cr>
"noremap <special> <leader>sv :vsp<cr>
"noremap <special> <leader>m  :new<cr>
"noremap <special> <leader>n  :vnew<cr>
noremap <special> <c-w>m  :vnew<cr>
noremap <special> <c-w><c-m>  :vnew<cr>

" resize {{{3
noremap <special> <LEFT> <c-w>>
noremap <special> <RIGHT> <c-w><
noremap <special> <UP> <c-w>+
noremap <special> <DOWN> <c-w>-

" movement  {{{2
noremap <special> <c-k> <c-w><c-j>
noremap <special> <c-i> <c-w><c-k>
noremap <special> <c-j> <c-w><c-h>
noremap <special> <c-l> <c-w><c-l>

" FOLDS {{{1
" Alias for recursive folds
noremap z<leader>o zR
noremap z<leader>c zM

noremap zi zk
noremap zk zj
" Its a bit annoying to have 'zi' 'zk' to move folds up and down but to have
" the inverted combination for start and end of fold as '[z' ']z', where 'z'
" comes after the direction '[' (I guess vim does this for consistency with
" movement commands such as '[[', '[]', etc.
noremap z[ [z
noremap z] ]z

" EXIT AND SAVE {{{1
noremap <special> <leader>ss :w<cr>
noremap <special> <leader>sq :wq<cr>
noremap <special> <leader>aq :qa<cr>
noremap <special> <leader>q :q<cr>
noremap <special> <leader>Q :q!<cr>
noremap <special> <leader>s :w<cr>

" MODE SWITCHING  {{{1
" `   : normal
" 1   : visual line
" 2   : visual block
" 3   : virtual replace
" 4   : replace
nnoremap <special> <leader>` <esc>i
vnoremap <special> <expr> <leader>` mode()=~'\cv' ? '<esc>i' : 'I'
onoremap <special> <leader>` <esc>i

noremap <special> <leader><leader>` <esc>a
noremap <special> <leader>1 <esc>v
noremap <special> <leader>2 <esc><c-v>
noremap <special> <leader>3 <esc>V
noremap <special> <leader>8 <esc>gR
noremap <special> <leader>9 <esc>R

noremap <special> <leader><leader>~ <esc>A
noremap <special> <leader>~ <esc>I

vnoremap <special> 1<leader> <esc>
vnoremap <special> 2<leader> <esc>
vnoremap <special> 3<leader> <esc>

inoremap <special> `<leader> <esc>

noremap <special> 1 <esc>v
noremap <special> 2 <esc><c-v>
noremap <special> 3 <esc>gR
noremap <special> 4 <esc>R

" OPERATORS {{{1
" delete {{{2
" Deleting is actually deleting: move text into blackhole register "_
noremap d "_d
nnoremap dd "_dd

" copy, cut, change {{{2
" set clipboard^=unnamedplus to mirror the double quote register with the plus
" register (X11 system clipboard): i.e. no need to append "+ to the commands
" (which bug out using other registers anyway)
" c(opy) is y(ank}
noremap c y
noremap C Y

" Set g:copy2mark_linewise to 0 to copy to the exact position
nnoremap <special> <c-c> :set opfunc=moistfunc#copy2markm<CR>g@
vnoremap <special> <c-c> :<C-U>call moistfunc#copy2markm(visualmode(), 1)<CR>
nnoremap <special> <c-c>c :call moistfunc#copyline2markm()<cr>
noremap c y
noremap C Y
" register (X11 system clipboard): i.e. no need to append "+ to the commands
" (which bug out using other registers anyway)
" c(opy) is y(ank}
noremap c y
noremap C Y

" Let cutting be actually cutting (Vim deleting is cutting)
noremap x d
" a(lter) is c(hange)
nnoremap a c
nnoremap aa cc

" around and alter share the same key
" Using alter during visual selection is rare
vnoremap A c

" replace (not really an operator) is s(ubstitute)
noremap s r

" MISCELLANEOUS {{{1
" Toggle (by !) highligh search and echo result (by ?)
nnoremap <leader>/ :set hlsearch! hlsearch?<cr>
nnoremap hi :set ignorecase! ignorecase?<cr>

" Put
"noremap v "+p
"noremap V "+P
nnoremap v p
nnoremap V P

" Redo
nnoremap r u
nnoremap R <c-r>

" Reverse direction
vnoremap r o

" Rebind 'i(nside)' (i) to 'w(ithin)'
onoremap w i
vnoremap w i

" Search forward word under cursor
" NOTE: <cr> is codes for the same as <c-m>
noremap <CR> *
" Search backwards word under cursor
noremap <BS> #

" jumplist movement
nnoremap <c-b> <c-o>
nnoremap <c-n> <c-i>

nnoremap <leader>i O
nnoremap <leader>k o

nnoremap hJ J
vnoremap hJ J

" PLUGINS {{{1
inoremap <special> <c-d> <c-o>:YcmCompleter GetDoc<cr>
nnoremap <special> <c-d> :YcmCompleter GetDoc<cr>
inoremap <c-y> <esc>:call moistfunc#toggleYCM()<cr>a


inoremap <special> <c-t> <C-R>=EasyCloseTag()<CR><C-R>=SetCursor()<cr>
nnoremap <special>  <F8> :TagbarToggle<cr>

nnoremap <special> <C-s> :SyntasticCheck<cr>
nnoremap <special> <C-h> :lopen<cr>

nmap <F8> :TagbarToggle<cr>

let g:UltiSnipsJumpBackwardTrigger =  "<c-j>"
let g:UltiSnipsJumpForwardTrigger =  "<c-l>"
inoremap <c-x> <C-R>=(moistfunc#ultisnips_expand_jump() > 0)?"":"\n"<CR>

nmap Sa <plug>searchAndAppend
vmap Sa <plug>searchAndAppend
nmap Si <plug>searchAndInsert
vmap Si <plug>searchAndInsert
nmap Sd <plug>searchAndDelete
vmap Sd <plug>searchAndDelete
nmap \\ <plug>line2search
nmap \ <plug>copy2search
vmap \ <plug>copy2search

let g:moist_textobj_map = {
            \ "PrevFuncOpen"    : [ "[[" ],
            \ "NextFuncOpen"    : [ "]]" ],
            \ "NextFuncClose"   : [ "][" ],
            \ "PrevFuncClose"   : [ "[]" ],
            \ "BlockUpStart"    : [ "bi" ],
            \ "BlockUpEnd"      : [ "Bi" ],
            \ "BlockDownStart"  : [ "bk" ],
            \ "BlockDownEnd"    : [ "Bk" ],
            \ "BlockNextStart"  : [ "bl" ],
            \ "BlockNextEnd"    : [ "Bl" ],
            \ "BlockPrevStart"  : [ "bj" ],
            \ "BlockPrevEnd"    : [ "Bj" ],
            \ "AroundFunc"      : [ "af" ],
            \ "InsideFunc"      : [ "wf" ],
            \ "InsideBlock"     : [ "wb" ],
            \ "AroundBlock"     : [ "ab" ]
            \ }


" VIMDIFF {{{1
" In diff mode quite with a non zero mode if not saved and exited so changes
" are not executed (commited, merged, pushed, etc...)
" BUG: the <buffer> argument make the mapping apply for one buffer and not
" both buffers. When vimdiff-ing the plugins are not reread. Solution might to
" implement it with an autocommand ('winenter' ahd check if '&diff' is set).
if &diff
    nnoremap <silent>  q :cq!<cr>
    nnoremap <silent> aq :cq!<cr>
    nnoremap <silent> Aq :cq!<cr>
endif

" vim: ts=8 sw=4 sts=4 et foldenable foldmethod=marker foldcolumn=1 foldlevel=0
