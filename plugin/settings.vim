" Setting global variables first and enabling options later so plugins
" responding to enabling options have the correct global variable settings.

" PLUGIN OPTIONS/VARIABLES {{{1
" autoindex {{{2
let g:moist_man_colorscheme = "pablo"
let g:moist_man_cursorline =  236
let g:autoindex_up = "i"
let g:autoindex_down = "k"

" YouCompleteMe {{{2
"let g:ycm_autoclose_preview_window_after_insert = 1
"let g:ycm_autoclose_preview_window_after_completion = 1
set completeopt-=preview
"let g:ycm_add_preview_to_completeopt = 0 " --> YcmCompleter GetDoc
let g:ycm_always_populate_location_list = 1
let g:ycm_min_num_of_chars_for_completion = 2
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'
let g:ycm_auto_hover = ''
let g:ycm_confirm_extra_conf = 0

"let g:EclimCompletionMethod = 'omnifunc'

" syntastic {{{2
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_always_populate_loc_list = 1

" Gutentags / ctags {{{2
let g:gutentags_cache_dir = $HOME . '/.vimtags'
let g:gutentags_ctags_extra_args=['-R', '-u', '--fields=+iaS', '--extra=+q']
let g:gutentags_enabled = 0

let g:ctagsargs=['-R', '--c-kinds=+1vfl', '--fields=+iaS', '--extra=+q']

" Airline {{{2
"" Enable the list of buffers at top
let g:airline#extensions#tabline#enabled = 1
"" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Apprentice {{{2
" default colorscheme
set cursorline
colorscheme apprentice

" moist_man {{{2
let g:moist_man_header_cutoff = 400
let g:autoclose_size_height = 10

" deleted plugins {{{3
" ctrlp
"let g:ctrlp_cmd= 'CtrlPBuffer'
"let g:ctrlp_cmd = 'g:ctrlp_map'
" See @bundle/ctrlp/autoload/ctrlp.vim:let 'let [s:pref, s:bpref, s:opts, s:new_opts, s:lc_opts] ='
" for all commands available

" GENERAL VIM OPTIONS {{{1
set nocompatible

filetype plugin indent on
syntax on

set omnifunc=syntaxcomplete#Complete

augroup remove_trailing_spaces
    au!
    au BufWritePre * :call moistfunc#RemoveTrailingSpace()
augroup END

" X does not get the contents of the clipboard, but request from the owning
" process when asked for the clipboard. xclip does not work on suspend vim
" because it will suspend xclip as well. With 'disown' xclip exits
" immediately.
" autocmd VimLeave,VimSuspend * call system("xclip -i -selection clipboard", getreg('+'))


" Space and tabbing {{{2
" Defaults, overwrite in ftplugin for specfic filetypes
set tabstop=4
set shiftwidth=4
set expandtab

" File browsing {{{2
" Autocompletes from pwd recursively
set path+=**
" Show possible completions on a status line
set wildmenu

" Registers/Operators {{{2
" Shadow unspecifed registers with the unnamedplus register ("+, i.e. systems
" clipboard)
set clipboard^=unnamedplus

" tilde as operator
set tildeop

" Visual settings {{{2
" Lines below and above the cursor
set scrolloff=18

" Splits {{{2
set splitbelow
set splitright

set relativenumber
set number
set hlsearch
set incsearch
set nowrap

" Debug {{{2
" Prevent background bleed in 256 color in tmux/screen (TERM=xterm-256color)
" if t_ut is non empty usees the terminal background to clear the screen
set t_ut=

" Optimize rendering {{{2

" Cursor might lag behind when using character movement commands when this
" option is set, but overall rendering is optimized (late rendering)
set lazyredraw

" Miscellanous
" Normal backspace behavoir
set backspace=indent,eol,start

" Set per file
"set foldmethod=syntax

" modeline {{{1
" vim:tw=78:ts=8:ft=vim:norl:foldmethod=marker
