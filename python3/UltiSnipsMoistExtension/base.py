def FilterComments(commentTag, snip, which):
    import vim

    if "KeepMoistExtraHelpInSnippet" in vim.vars:
        return

    from re import match,compile
    MoistHelpPattern = compile("^\s*" + commentTag)
    currentpos = snip.snippet_start[0]
    endline = snip.snippet_end[0]
    while currentpos <= endline:
        if MoistHelpPattern.match(snip.buffer[currentpos]):
            del snip.buffer[currentpos]
            endline -= 1
            currentpos -= 1
        currentpos += 1

    snip.cursor.set(endline, snip.cursor[1])
