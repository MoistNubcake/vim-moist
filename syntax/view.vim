" This cannot be placed in after/syntax because AnsiEsc does not follow the
" regular syntax plugin loading but creates command at once of the last
" possible settings.

autocmd BufEnter * ++once hi! ansiBlueFg ctermfg=12
autocmd VimEnter * ++once hi! ansiBlueFg ctermfg=12
